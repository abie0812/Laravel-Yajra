<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <title>Yajra DataTable</title>
</head>
<body>
    <div class="container">
        <br>
        <h3 class="text-center">Datatables Server Side Processing in Laravel</h3>
        <br>
        <table id="student_table" class="table table-striped table-bordered" style="width:100%">
            <thead>
                <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Biography</th>
                </tr>
            </thead>
        </table>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#student_table').DataTable( {
                "processing": true,
                "serverSide": true,
                "ajax": "{{ route('ajaxdata.getdata') }}",
                "columns": [
                    {"data" : "first_name"},
                    {"data" : "last_name"},
                    {"data" : "biography"}
                ]
            } );
        } );
    </script>
</body>
</html>
