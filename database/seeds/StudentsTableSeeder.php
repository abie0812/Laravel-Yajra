<?php

use App\Student;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now()->toDateTimeString();

        Student::insert([
           ['first_name'=>'Bambang', 'last_name'=>'Pamungkas', 'biography' => 'Artist', 'created_at'=>$now, 'updated_at'=>$now],
           ['first_name'=>'Lionel', 'last_name'=>'Messi', 'biography' => 'Artist', 'created_at'=>$now, 'updated_at'=>$now],
           ['first_name'=>'Christiano', 'last_name'=>'Ronaldo', 'biography' => 'Artist', 'created_at'=>$now, 'updated_at'=>$now],
           ['first_name'=>'Marion', 'last_name'=>'Jola', 'biography' => 'Artist', 'created_at'=>$now, 'updated_at'=>$now],
           ['first_name'=>'Chandra', 'last_name'=>'Putra', 'biography' => 'Artist', 'created_at'=>$now, 'updated_at'=>$now],
           ['first_name'=>'Sandra', 'last_name'=>'Olga', 'biography' => 'Artist', 'created_at'=>$now, 'updated_at'=>$now],
           ['first_name'=>'Obbie', 'last_name'=>'Messakh', 'biography' => 'Artist', 'created_at'=>$now, 'updated_at'=>$now],
           ['first_name'=>'Hamish', 'last_name'=>'Daud', 'biography' => 'Artist', 'created_at'=>$now, 'updated_at'=>$now],
           ['first_name'=>'Isyana', 'last_name'=>'Sarasvati', 'biography' => 'Artist', 'created_at'=>$now, 'updated_at'=>$now],
           ['first_name'=>'Raisa', 'last_name'=>'Andriana', 'biography' => 'Artist', 'created_at'=>$now, 'updated_at'=>$now],
        ]);
    }
}
